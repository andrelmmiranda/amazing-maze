
const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];


// const map = [
//     "WWWWWWWWWWWWWWWWWWWWW",
//     "W         W     W W W",
//     "W W W WWW WWWWW W W W",
//     "S W W   W           F",
//     "WWWWWWWWWWWWWWWWWWWWW",
// ];

let ground = document.getElementById('ground');

const createMap = (array) => {
    let wall;
    let brick;
    let pathway;
    let startPosition;
    let endPosition;

    for(let i = 0; i < array.length; i++){
        wall = document.createElement('div');
        wall.classList.add('wall');
        for(let j = 0; j < array[i].length; j++){
            if(array[i][j] === "W"){
            brick = document.createElement('div');
            brick.classList.add('floor');
            brick.classList.add('brick');
            wall.appendChild(brick);  
            } else if(array[i][j] === "S"){
            startPosition = document.createElement('div');
            startPosition.classList.add('floor')
            startPosition.classList.add('start');
            wall.appendChild(startPosition);
            } else if(array[i][j] === "F"){
            endPosition = document.createElement('div');
            endPosition.classList.add('floor')
            endPosition.classList.add('end');
            wall.appendChild(endPosition);
            } else{
            pathway = document.createElement('div');
            pathway.classList.add('floor')
            pathway.classList.add('pathway');
            wall.appendChild(pathway);
            }
        }
        ground.appendChild(wall);
    }
};

createMap(map);

function placeStart(arr){
    let startPlace;
    let counter = 0;

    for(let i = 0; i < arr.length; i++){
        for(let j = 0; j < arr[i].length; j++){
            counter++;

            if(arr[i][j] == "S"){
                startPlace = counter;
                return startPlace;
            }
        }
    }

}

function placeEnd(arr){
    let endPlace;
    let counter = 0;

    for(let i = 0; i < arr.length; i++){
        for(let j = 0; j < arr[i].length; j++){
            counter++;

            if(arr[i][j] == "F"){
                endPlace = counter;
                return endPlace;
            }
        }
    }
}

function placePlayer(position){
    let fatherElement = document.querySelectorAll('.floor')[position-1];

    let caracter = document.createElement('div');
    caracter.setAttribute('id','player');
    
    fatherElement.appendChild(caracter);
}

placePlayer(placeStart(map));

let currentPosition = placeStart(map)-1;
let finalPosition = currentPosition;

document.addEventListener('keydown', function(event){
    let getStart = document.querySelectorAll('.floor')[finalPosition].lastElementChild;
    let passPosition;
    const keyName = event.key;


    if(keyName === "ArrowDown"){
        passPosition = finalPosition;
        passPosition += 21;
    } else if(keyName === "ArrowUp"){
        passPosition = finalPosition;
        passPosition -= 21;
    } else if(keyName === "ArrowLeft"){
        passPosition = finalPosition;
        passPosition -= 1;
    } else if(keyName === "ArrowRight"){
        passPosition = finalPosition;
        passPosition += 1;
    }

    if(!invalidMove(passPosition)){
        let getEnd = document.querySelectorAll('.floor')[passPosition];
        getEnd.appendChild(getStart);
        currentPosition = passPosition
    } else{
        return;
    }
       finalPosition = currentPosition;

    winner(map);
});

function winner(arr){
    let place = document.querySelectorAll('.floor')[placeEnd(arr)-1].lastElementChild.id;

    if(place === 'player'){
        let message = document.querySelector('.hide');
        message.classList.remove('hide');

        return true;
    }
}

function invalidMove(position){
    let invalidMovement = document.querySelectorAll('.floor')[position].classList.contains('brick')

    return invalidMovement;
}